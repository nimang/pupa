package com.nimang.pupa.dbExtends;

public class DBConstants {
    // 精度最多限制到亿位
    public static final Integer PRECISION_LIMIT = 9;
    public static final Integer PRECISION_LIMIT_FLOAT = 11;
}
