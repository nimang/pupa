package com.nimang.pupa.dbExtends.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.dbExtends.mysql.entity.MysqlColumns;


/**
 * @author JustHuman
 * @date 2023-04-18
 */
public interface MysqlColumnsMapper extends BaseMapper<MysqlColumns> {

}