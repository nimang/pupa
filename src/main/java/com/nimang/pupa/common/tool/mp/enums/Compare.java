package com.nimang.pupa.common.tool.mp.enums;

/**
 * 比较操作枚举
 * @author 方志刚
 * @date 2023/2/22 13:52
 */
public enum Compare {

    EQ,
    NE,
    GT,
    GE,
    LT,
    LE,
    BETWEEN,
    NOT_BETWEEN,
    LIKE,
    NOT_LIKE,
    LIKE_LEFT,
    LIKE_RIGHT,
    NOT_LIKE_LEFT,
    NOT_LIKE_RIGHT,
    IN,
    NOT_IN,
    IS_NULL,
    IS_NOT_NULL,
    OR,
    AND

}
