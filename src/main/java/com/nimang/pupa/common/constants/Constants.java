package com.nimang.pupa.common.constants;

/**
 * 通用常量信息
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String ASC = "asc";
    public static final String DESC = "desc";

    public static final Integer CHAR_INT_ZERO               = 0;
    public static final String CHAR_SPACE                   = " ";
    public static final String CHAR_BLANK                   = "";
    public static final String CHAR_COMMA                   = ",";
    public static final String CHAR_POINT                   = ".";
    public static final String CHAR_BAR                     = "-";
    public static final String CHAR_TILDE                   = "~";
    public static final String CHAR_AT                      = "@";
    public static final String CHAR_UNDERSCORE              = "_";
    public static final String CHAR_ASTERISK                = "*";
    public static final String CHAR_PERCENT                 = "%";
    public static final String CHAR_VERTICAL_DIVIDE         = "|";
    public static final String CHAR_AMPERSAND               = "&";
    public static final String CHAR_AND                     = "&&";
    public static final String CHAR_SPLIT_POINT             = "\\.";
    public static final String CHAR_SPLIT_SLASH		        ="/";
    public static final String EXCEL_2003_DOWN		        =".xls";
    public static final String EXCEL_2007_UP		        =".xlsx";
    public static final String FILE_ZIP					    =".zip";
    public static final String NULL_STR                     = "null";

    public static final String ESCAPE_CHAR_TEB              = "\t";
    public static final String ESCAPE_CHAR_WRAP             = "\n";

    public static final int ZERO                = 0;
    public static final String ZERO_STR         = "0";
    public static final int ONE                 = 1;
    public static final String ONE_STR          = "1";
    public static final int TWO                 = 2;
    public static final String TWO_STR          = "2";
    public static final int THREE               = 3;
    public static final String THREE_STR        = "3";
    public static final int FOUR                = 4;
    public static final String FOUR_STR         = "4";
    public static final int FIVE                = 5;
    public static final String FIVE_STR         = "5";
    public static final int SIX                 = 6;
    public static final String SIX_STR          = "6";
    public static final int SEVEN               = 7;
    public static final String SEVEN_STR        = "7";
    public static final int EIGHT               = 8;
    public static final String EIGHT_STR        = "8";
    public static final int NINE                = 9;
    public static final String NINE_STR         = "9";
    public static final int TEN                 = 10;
    public static final String TEN_STR          = "10";
    public static final int HUNDRED             = 100;
    public static final String HUNDRED_STR      = "100";
    public static final int THOUSAND            = 1000;
    public static final String THOUSAND_STR     = "1000";

    public static final Long ADMIN_ID           = 1L;
}
