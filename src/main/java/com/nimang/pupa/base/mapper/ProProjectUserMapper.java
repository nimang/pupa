package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.ProProjectUser;


/**
 * 项目成员-Mapper
 * @author JustHuman
 * @date 2023-04-26
 */
public interface ProProjectUserMapper extends BaseMapper<ProProjectUser> {

}