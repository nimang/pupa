package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.ProColumnType;


/**
 * 列类型-Mapper
 * @author JustHuman
 * @date 2023-09-07
 */
public interface ProColumnTypeMapper extends BaseMapper<ProColumnType> {

}