package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.SysUser;


/**
 * 用户信息-Mapper
 * @date 2023-04-12
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}