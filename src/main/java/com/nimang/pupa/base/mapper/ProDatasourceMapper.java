package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.ProDatasource;


/**
 * 数据源-Mapper
 * @author JustHuman
 * @date 2023-04-26
 */
public interface ProDatasourceMapper extends BaseMapper<ProDatasource> {

}