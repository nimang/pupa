package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.ProField;


/**
 * 表字段-Mapper
 * @author JustHuman
 * @date 2023-04-26
 */
public interface ProFieldMapper extends BaseMapper<ProField> {

}