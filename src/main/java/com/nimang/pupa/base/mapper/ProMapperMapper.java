package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.ProMapper;


/**
 * 数据映射-Mapper
 * @author JustHuman
 * @date 2023-08-09
 */
public interface ProMapperMapper extends BaseMapper<ProMapper> {

}