package com.nimang.pupa.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nimang.pupa.base.entity.ProConfig;


/**
 * 配置-Mapper
 * @author JustHuman
 * @date 2023-04-21
 */
public interface ProConfigMapper extends BaseMapper<ProConfig> {

}