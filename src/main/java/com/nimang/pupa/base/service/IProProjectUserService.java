package com.nimang.pupa.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nimang.pupa.base.entity.ProProjectUser;

/**
 * 项目成员-数据服务接口
 * @author JustHuman
 * @date 2023-04-26
 */
public interface IProProjectUserService extends IService<ProProjectUser>{

}
