package com.nimang.pupa.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nimang.pupa.base.entity.ProColumnType;

/**
 * 列类型-数据服务接口
 * @author JustHuman
 * @date 2023-09-08
 */
public interface IProColumnTypeService extends IService<ProColumnType>{

}
