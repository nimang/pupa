package com.nimang.pupa.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nimang.pupa.base.entity.ProDatasource;

/**
 * 数据源-数据服务接口
 * @author JustHuman
 * @date 2023-04-26
 */
public interface IProDatasourceService extends IService<ProDatasource>{

}
