package com.nimang.pupa.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nimang.pupa.base.entity.ProTemplate;

/**
 * 模板-数据服务接口
 * @author JustHuman
 * @date 2023-04-21
 */
public interface IProTemplateService extends IService<ProTemplate>{

}
